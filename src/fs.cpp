#include <boost/filesystem.hpp>
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <string>
#include "utils.hpp"

namespace fs = boost::filesystem;

typedef std::pair<std::vector<std::string>, std::vector<std::string> > strings_pair;

strings_pair get_all_child_files_recursively_and_separate(const fs::path &path);
void usage(char* bin_name);

int main(int argc, char *argv[]) {
	if (argc != 2) {
		usage(argv[0]);
		return 1;
	}

	fs::path p(argv[1]);

	if (!fs::exists(p)) {
		std::cout << argv[1] << ": No such file or directory\n";
		usage(argv[0]);
		return 1;
	}

	if (!fs::is_directory(p)) {
		std::cout << argv[1] << " is not a directory\n";
	}

	strings_pair result = get_all_child_files_recursively_and_separate(p);
	std::sort(result.first.begin(), result.first.end());
	std::sort(result.second.begin(), result.second.end());
	
	for (std::vector<std::string>::iterator it = result.first.begin(); it != result.first.end(); ++it) {
		std::cout << *it << std::endl;
	}

	for (std::vector<std::string>::iterator it = result.second.begin(); it != result.second.end(); ++it) {
		std::cout << *it << std::endl;
	}
    
    return 0;
}


strings_pair get_all_child_files_recursively_and_separate(const fs::path &path) {
	std::vector<std::string> result_flat;
	std::vector<std::string> result_deep;

	for (fs::directory_iterator it(path), end; it != end; ++it) {
		if (fs::is_regular_file(it->path())) {
			result_flat.push_back(relativePath(*it, path).native());
		}
	}
	for (fs::recursive_directory_iterator it(path), end; it != end; ++it) {
        if (fs::is_regular_file(it->path())) {
            std::string potential_path = relativePath(*it, path).native();
            if (std::find(result_flat.begin(), result_flat.end(), potential_path) == result_flat.end()) {
            	result_deep.push_back(potential_path);
            }
        }
    }
    return std::make_pair(result_flat, result_deep);
}


void usage(char* bin_name) {
	std::cout << "Usage:\n";
	std::cout << bin_name << " path/to/directory\n";
}