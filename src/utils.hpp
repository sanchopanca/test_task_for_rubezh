#include <boost/filesystem.hpp>

boost::filesystem::path relativePath(const boost::filesystem::path &path, const boost::filesystem::path &relative_to);