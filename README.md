Dependecies:
```
boost-filesystem
```

To compile input (if you have Linux):
``` bash
g++ src/fs.cpp src/utils.cpp -lboost_filesystem -lboost_system
```
To run:
``` bash
./a.out path/to/directory
```
